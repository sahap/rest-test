<?php
require 'PaymentUtil.php';
class PaymentDao {

	public function findAllPayments(){
		$obj_lines = file("./db/payment_fact");
		$payment_arr=array();
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id = (int)$obj_arr[0];
		 $code = $obj_arr[1];
		 $customer_id = (int) $obj_arr[2];
		 $amount = $obj_arr[3];
		 $currency_code = $obj_arr[4];
		 $fee_amount = $obj_arr[5];
		 $description = ($obj_arr[6] === "") ? null : $obj_arr[6];
		 $status = $obj_arr[7];
		 $payment_method = explode(',',$obj_arr[8]);
		 $created = (int)$obj_arr[9];
		 $canceled = (int)$obj_arr[10];
		 $cancellation_reason = ($obj_arr[11] === "") ? null : $obj_arr[11];
		 $capture_method = $obj_arr[12];
		 $receipt_email = ($obj_arr[13] === "") ? null : $obj_arr[13];
		 $review = ($obj_arr[14] === "") ? null : $obj_arr[14];		 
		 $amount = $obj_arr[15];		
		 $destination = preg_replace("/\r|\n/", "", $obj_arr[16]);
		 
		 $transfer_data = array("amount"=>$amount, "destination"=>$destination);
		 $payment_arr_itm = array("id"=>$id, "code"=>$code, "customer_id"=>$customer_id, "amount"=>$amount, "currency_code"=>$currency_code, "fee_amount"=>$fee_amount, "description"=>$description, "status"=>$status, "payment_method"=>$payment_method, "created"=>$created, "canceled"=>$canceled, "cancellation_reason"=>$cancellation_reason, "capture_method"=>$capture_method, "receipt_email"=>$receipt_email, "review"=>$review, "transfer_data"=>$transfer_data);
		 array_push($payment_arr, $payment_arr_itm);
		}
		return $payment_arr;
	}
	
	public function findPaymentById($id){
		if($id > 1000){
			$obj_lines = file("./db/payment_fact_data");	
		}
		else{
			$obj_lines = file("./db/payment_fact");	
		}			
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
			if($obj_arr[0] == $id){
			 $id = (int)$obj_arr[0];
			 $code = $obj_arr[1];
			 $customer_id = (int) $obj_arr[2];
			 $amount = $obj_arr[3];
			 $currency_code = $obj_arr[4];
			 $fee_amount = $obj_arr[5];
			 $description = ($obj_arr[6] === "") ? null : $obj_arr[6];
			 $status = $obj_arr[7];
			 $payment_method = explode(',',$obj_arr[8]);
			 $created = (int)$obj_arr[9];
			 $canceled = (int)$obj_arr[10];
			 $cancellation_reason = ($obj_arr[11] === "") ? null : $obj_arr[11];
			 $capture_method = $obj_arr[12];
			 $receipt_email = ($obj_arr[13] === "") ? null : $obj_arr[13];
			 $review = ($obj_arr[14] === "") ? null : $obj_arr[14];		 
			 $amount = $obj_arr[15];		
			 $destination = preg_replace("/\r|\n/", "", $obj_arr[16]);
			 break;
			}			
		}
		
		if($customer_id){
		 $transfer_data = array("amount"=>$amount, "destination"=>$destination);
		 $payment_arr = array("id"=>$id, "code"=>$code, "customer_id"=>$customer_id, "amount"=>$amount, "currency_code"=>$currency_code, "fee_amount"=>$fee_amount, "description"=>$description, "status"=>$status, "payment_method"=>$payment_method, "created"=>$created, "canceled"=>$canceled, "cancellation_reason"=>$cancellation_reason, "capture_method"=>$capture_method, "receipt_email"=>$receipt_email, "review"=>$review, "transfer_data"=>$transfer_data);
		}
		else{
			$payment_arr = array("message"=>"Error! No payment found");
			}		
		return $payment_arr;
	}
	
	public function insertPayment(Array $input){
		$obj_lines = file("./db/payment_fact_data");
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];
		}
		if(!$id_num or $id_num == "")
		{ $id_num = 1001; }
		else{ $id_num ++;}
		$util = new PaymentUtil();		
        
		 $code = $util->generatePayCode(); // generate pay_code
		 $customer_id = $input['customer_id'];
		 $amount = $input['amount'];
		 $currency_code = $input['currency_code'];
		 $amount = $input['amount'];
		 $fee_amount = $util->calcFeeAmt($amount); //fee amount
		 $description = $input['description'];
		 $status =  $util->findStatus(); // status
		 $payment_method = "card";
		 $amount = $input['amount'];		 
		 $created = time();		 
		 $capture_method = "automatic";
		 $receipt_email = $input['receipt_email'];
		 $review = $input['review'];
		 $destination=$input['transfer_data']['destination'];
		 
	$file_data = fopen("./db/payment_fact_data", "a");
    fwrite($file_data, "$id_num|$code|$customer_id|$amount|$currency_code|$fee_amount|$description|$status|$payment_method|$created|||$capture_method|$receipt_email|$review|$amount|$destination\n");
    fclose($file_data);
    return array("message"=>"Success", "id"=>$id_num);		
	}
	
	public function cancelPayment($id, Array $input){
		$obj_lines = file("./db/payment_fact_data");
		$new_obj = fopen("./db/payment_fact_data", "w");			
		 $canceled = $input['canceled'];
		 $cancellation_reason = $input['cancellation_reason'];		 
		
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];				 
		 if($id_num == $id){			   
             $res = fwrite($new_obj, "$obj_arr[0]|$obj_arr[1]|$obj_arr[2]|$obj_arr[3]|$obj_arr[4]|$obj_arr[5]|$obj_arr[6]|$obj_arr[7]|$obj_arr[8]|$obj_arr[9]|$canceled|$cancellation_reason|$obj_arr[12]|$obj_arr[13]|$obj_arr[14]|$obj_arr[15]|$obj_arr[16]\n");
			}else{
			 fwrite($new_obj, "$single_line");
			}
		 }
		 fclose($new_obj);
		 if($res){
			return array("message"=>"cancel successful");	
		 }
		 else{
			return array("message"=>"Error! nothing to cancel");	 
		 }		
	 }
	 
}
?>