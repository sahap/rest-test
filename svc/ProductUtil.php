<?php

class ProductUtil{
	
	public function generateSku(){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$code = '';
		for ($i = 0; $i < 7; $i++) {
			$code .= $characters[rand(0, $charactersLength - 1)];
		}
		return 'sku_'.$code;	
	}	
	
}
?>