<?php
require 'ProductUtil.php';
class ProductDao {

	public function findAllProducts(){
		$obj_lines = file("./db/product_master");
		$product_arr=array();
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id = (int) $obj_arr[0];
		 $sku =  $obj_arr[1];
		 $active = filter_var($obj_arr[2], FILTER_VALIDATE_BOOLEAN);
		 $name = $obj_arr[3];
		 $description = $obj_arr[4];		
   		 $size = ($obj_arr[5] === "") ? null : $obj_arr[5];
		 $gender = ($obj_arr[6] === "") ? null : $obj_arr[6];	 
		 $created = (int)$obj_arr[7];
		 $currency = $obj_arr[8];		 
		 $quantity = (int) $obj_arr[9];
		 $type = $obj_arr[10];
		 $value = $obj_arr[11];		 
		 $width = ($obj_arr[12] === "") ? null : (int)$obj_arr[12];
		 $height = ($obj_arr[13] === "") ? null : (int)$obj_arr[13];
		 $length = ($obj_arr[14] === "") ? null : (int)$obj_arr[14]; 
		 $weight = (int)$obj_arr[15];		 
		 $price = $obj_arr[16];
		 $shippable = ($obj_arr[17] === "") ? null : filter_var($obj_arr[17], FILTER_VALIDATE_BOOLEAN);
		 $updated = ($obj_arr[18] === "") ? null : (int)$obj_arr[18];
		 $season_label = preg_replace("/\r|\n/", "", $obj_arr[19]);	
		 $season_label = ($season_label === "") ? null : $season_label;
		 
		 $attributes_arr = array("size"=>$size, "gender"=>$gender);
		 $inventory_arr = array("quantity"=>$quantity, "type"=>$type, "value"=>$value);
		 $package_dimensions_arr = array("width"=>$width, "height"=>$height, "length"=>$length, "weight"=>$weight);
		 
		 $product_arr_itm = array("id"=>$id, "sku"=>$sku, "active"=>$active, "name"=>$name, "description"=>$description, "attributes"=>$attributes_arr, "created"=>$created, "currency"=>$currency, "inventory"=>$inventory_arr, "package_dimensions"=>$package_dimensions_arr, "price"=>$price, "shippable"=>$shippable, "updated"=>$updated, "season_label"=>$season_label);
		 array_push($product_arr, $product_arr_itm);
		}
		return $product_arr;
	}
	
	public function findProduct($id){
		if($id > 1000){
		$obj_lines = file("./db/product_master_data");
		}
		else{
		$obj_lines = file("./db/product_master");		
		}			
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
			if($obj_arr[0] == $id){
			 $id = (int) $obj_arr[0];
			 $sku =  $obj_arr[1];
			 $active = filter_var($obj_arr[2], FILTER_VALIDATE_BOOLEAN);
			 $name = $obj_arr[3];
			 $description = $obj_arr[4];		
			 $size = ($obj_arr[5] === "") ? null : $obj_arr[5];
			 $gender = ($obj_arr[6] === "") ? null : $obj_arr[6];	 
			 $created = (int)$obj_arr[7];
			 $currency = $obj_arr[8];		 
			 $quantity = (int) $obj_arr[9];
			 $type = $obj_arr[10];
			 $value = $obj_arr[11];		 
			 $width = ($obj_arr[12] === "") ? null : (int)$obj_arr[12];
			 $height = ($obj_arr[13] === "") ? null : (int)$obj_arr[13];
			 $length = ($obj_arr[14] === "") ? null : (int)$obj_arr[14]; 
			 $weight = (int)$obj_arr[15];		 
			 $price = $obj_arr[16];
			 $shippable = ($obj_arr[17] === "") ? null : filter_var($obj_arr[17], FILTER_VALIDATE_BOOLEAN);
			 $updated = ($obj_arr[18] === "") ? null : (int)$obj_arr[18];
			 $season_label = preg_replace("/\r|\n/", "", $obj_arr[19]);	
			 $season_label = ($season_label === "") ? null : $season_label;	 
			 break;
			}			
		}		
		if($sku){
		 $attributes_arr = array("size"=>$size, "gender"=>$gender);
		 $inventory_arr = array("quantity"=>$quantity, "type"=>$type, "value"=>$value);
		 $package_dimensions_arr = array("width"=>$width, "height"=>$height, "length"=>$length, "weight"=>$weight);
		 
		 $product_arr = array("id"=>$id, "sku"=>$sku, "active"=>$active, "name"=>$name, "description"=>$description, "attributes"=>$attributes_arr, "created"=>$created, "currency"=>$currency, "inventory"=>$inventory_arr, "package_dimensions"=>$package_dimensions_arr, "price"=>$price, "shippable"=>$shippable, "updated"=>$updated, "season_label"=>$season_label);
			}
			else{
			$product_arr = array("message"=>"Error! No task found");
			}		
		return $product_arr;
	}
	
	public function findLimitProducts($limit){
		$obj_lines = file("./db/product_master");
		$sliced_arr = array_slice($obj_lines,0,$limit);
		$product_arr=array();
		foreach($sliced_arr as $single_line){
		$obj_arr = explode("|", $single_line);			
		$id = (int) $obj_arr[0];
		 $sku =  $obj_arr[1];
		 $active = filter_var($obj_arr[2], FILTER_VALIDATE_BOOLEAN);
		 $name = $obj_arr[3];
		 $description = $obj_arr[4];		
   		 $size = ($obj_arr[5] === "") ? null : $obj_arr[5];
		 $gender = ($obj_arr[6] === "") ? null : $obj_arr[6];	 
		 $created = (int)$obj_arr[7];
		 $currency = $obj_arr[8];		 
		 $quantity = (int) $obj_arr[9];
		 $type = $obj_arr[10];
		 $value = $obj_arr[11];		 
		 $width = ($obj_arr[12] === "") ? null : (int)$obj_arr[12];
		 $height = ($obj_arr[13] === "") ? null : (int)$obj_arr[13];
		 $length = ($obj_arr[14] === "") ? null : (int)$obj_arr[14]; 
		 $weight = (int)$obj_arr[15];		 
		 $price = $obj_arr[16];
		 $shippable = ($obj_arr[17] === "") ? null : filter_var($obj_arr[17], FILTER_VALIDATE_BOOLEAN);
		 $updated = ($obj_arr[18] === "") ? null : (int)$obj_arr[18];
		 $season_label = preg_replace("/\r|\n/", "", $obj_arr[19]);	
		 $season_label = ($season_label === "") ? null : $season_label;
		
		 $attributes_arr = array("size"=>$size, "gender"=>$gender);
		 $inventory_arr = array("quantity"=>$quantity, "type"=>$type, "value"=>$value);
		 $package_dimensions_arr = array("width"=>$width, "height"=>$height, "length"=>$length, "weight"=>$weight);
		 
		 $product_arr_itm = array("id"=>$id, "sku"=>$sku, "active"=>$active, "name"=>$name, "description"=>$description, "attributes"=>$attributes_arr, "created"=>$created, "currency"=>$currency, "inventory"=>$inventory_arr, "package_dimensions"=>$package_dimensions_arr, "price"=>$price, "shippable"=>$shippable, "updated"=>$updated, "season_label"=>$season_label);
		 array_push($product_arr, $product_arr_itm);						
		}	
		return $product_arr;
	}
	
	
	public function insertProduct(Array $input){
		$obj_lines = file("./db/product_master_data");
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];
		}
		if(!$id_num or $id_num == "")
		{ $id_num = 1001; }
		else{ $id_num ++;}
		 $util = new ProductUtil();	
		 $sku = $util->generateSku();
		 $active = $input['active'];
		 $name = $input['name'];
		 $description = $input['description'];
		 $size = $input['attributes']['size'];
		 $gender = $input['attributes']['gender'];
		 $created = time();
		 $currency = $input['currency'];
		 $quantity = $input['inventory']['quantity'];
		 $type = $input['inventory']['type'];
		 $value = $input['inventory']['value'];
		 $width = $input['package_dimensions']['width'];
		 $height = $input['package_dimensions']['height'];
		 $length = $input['package_dimensions']['length'];
		 $weight = $input['package_dimensions']['weight'];
		 $price = $input['price'];
		 $shippable = $input['shippable'];
		 $season_label = $input['season_label'];
		 
	$file_data = fopen("./db/product_master_data", "a");
    fwrite($file_data, "$id_num|$sku|$active|$name|$description|$size|$gender|$created|$currency|$quantity|$type|$value|$width|$height|$length|$weight|$price|$shippable||$season_label\n");
    fclose($file_data);
    return array("message"=>"Success", "id"=>$id_num);		
	}
	
	public function updateProduct($id){
		$obj_lines = file("./db/product_master_data");
		$new_obj = fopen("./db/product_master_data", "w");	
		
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];	
		 	
		 if($id_num == $id){
			 $active = $obj_arr[2];	
			if($active == 1){
			   $active = 0;
		    }
		     else{$active = 1; }
             $res = fwrite($new_obj, "$obj_arr[0]|$obj_arr[1]|$active|$obj_arr[3]|$obj_arr[4]|$obj_arr[5]|$obj_arr[6]|$obj_arr[7]|$obj_arr[8]|$obj_arr[9]|$obj_arr[10]|$obj_arr[11]|$obj_arr[12]|$obj_arr[13]|$obj_arr[14]|$obj_arr[15]|$obj_arr[16]|$obj_arr[17]|$obj_arr[18]|$obj_arr[19]\n");
			}else{
			 fwrite($new_obj, "$single_line");
			}
		 }
		 fclose($new_obj);
		 if($res && $active){
			return array("message"=>"product is now active");	
		 }
		 else if($res && !$active){
			return array("message"=>"product is now inactive");	
		 }
		 else{
			return array("message"=>"Error! nothing to activate/de-activate");	 
		 }		
	 }
	 
	 public function removeProduct($id){
		$obj_lines = file("./db/product_master_data");
		$new_obj = fopen("./db/product_master_data", "w");				
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];		 
		 if($id_num != $id){	
             $res = fwrite($new_obj, "$single_line");
			}
		 }
		 fclose($new_obj);
		 if($res){
			return array("message"=>"delete successful");	
		 }
		 else{
			return array("message"=>"Error! nothing to delete");	 
		 }		 
	 }
}
?>