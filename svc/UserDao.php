<?php
class UserDao {

	public function findAllUsers(){
		$obj_lines = file(__DIR__."/db/user_master");
		$user_arr=array();
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id = (int) $obj_arr[0];
		 $name =  $obj_arr[1];
		 $phone =  $obj_arr[2];
		 $email = $obj_arr[3];
		 $company = $obj_arr[4];
		 $ssn = $obj_arr[5];
		 $street = $obj_arr[6];
		 $city = $obj_arr[7];
		 $zipcode = $obj_arr[8];
		 $country = $obj_arr[9];
		 $lat = $obj_arr[10];
		 $long = $obj_arr[11];
		 $dob = $obj_arr[12];
		 $gender = preg_replace("/\r|\n/", "", $obj_arr[13]);
		 
		 $user_arr_loc = array("lat"=> $lat, "long"=> $long);
		 $user_arr_addr = array("street"=> $street, "city"=> $city, "zipcode"=> $zipcode, "country"=> $country, "location"=> $user_arr_loc);		 
		 $user_arr_itm = array("id"=> $id, "name"=> $name, "phone"=> $phone, "email"=> $email, "company"=> $company, "ssn"=> $ssn, "address"=> $user_arr_addr, "dob"=>$dob, "gender"=>$gender);
		 array_push($user_arr, $user_arr_itm);
		}
		return $user_arr;
	}
	
	public function findUserById($id){
		$obj_lines = file(__DIR__."/db/user_master");		
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
			if($obj_arr[0] == $id){
			 $id = (int) $obj_arr[0];
			 $name =  $obj_arr[1];
			 $phone =  $obj_arr[2];
			 $email = $obj_arr[3];
			 $company = $obj_arr[4];
			 $ssn = $obj_arr[5];
			 $street = $obj_arr[6];
			 $city = $obj_arr[7];
			 $zipcode = $obj_arr[8];
			 $country = $obj_arr[9];
			 $lat = $obj_arr[10];
			 $long = $obj_arr[11];
			 $dob = $obj_arr[12];
			 $gender = preg_replace("/\r|\n/", "", $obj_arr[13]);
			 break;
			}
		}
		 $user_arr_loc = array("lat"=> $lat, "long"=> $long);
		 $user_arr_addr = array("street"=> $street, "city"=> $city, "zipcode"=> $zipcode, "country"=> $country, "location"=> $user_arr_loc);		 
		 $user_arr = array("id"=> $id, "name"=> $name, "phone"=> $phone, "email"=> $email, "company"=> $company, "ssn"=> $ssn, "address"=> $user_arr_addr, "dob"=>$dob, "gender"=>$gender);
		return $user_arr;
	}
	
	public function findUserByLimit($limit){
		$obj_lines = file(__DIR__."/db/user_master");
		$sliced_arr = array_slice($obj_lines,0,$limit);
		$user_arr=array();
		foreach($sliced_arr as $single_line){
			$obj_arr = explode("|", $single_line);
			 $id = (int) $obj_arr[0];
			 $name =  $obj_arr[1];
			 $phone =  $obj_arr[2];
			 $email = $obj_arr[3];
			 $company = $obj_arr[4];
			 $ssn = $obj_arr[5];
			 $street = $obj_arr[6];
			 $city = $obj_arr[7];
			 $zipcode = $obj_arr[8];
			 $country = $obj_arr[9];
			 $lat = $obj_arr[10];
			 $long = $obj_arr[11];
			 $dob = $obj_arr[12];
			 $gender = preg_replace("/\r|\n/", "", $obj_arr[13]);
			 
			 $user_arr_loc = array("lat"=> $lat, "long"=> $long);
			 $user_arr_addr = array("street"=> $street, "city"=> $city, "zipcode"=> $zipcode, "country"=> $country, "location"=> $user_arr_loc);		 
			 $user_arr_itm = array("id"=> $id, "name"=> $name, "phone"=> $phone, "email"=> $email, "company"=> $company, "ssn"=> $ssn, "address"=> $user_arr_addr, "dob"=>$dob, "gender"=>$gender);
			 array_push($user_arr, $user_arr_itm);
		}
		return $user_arr;
	}
}
?>