<?php
class ReaderDao {

	public function findAllReaders(){
		$obj_lines = file("./db/reader_dim");

		$reader_arr=array();
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id = (int) $obj_arr[0];
		 $node_id = $obj_arr[1];
		 $device_type = $obj_arr[2];
		 $device_sw_version = ($obj_arr[3] === "") ? null : $obj_arr[3];
		 $ip_address = $obj_arr[4];
		 $label = $obj_arr[5];
		 $lat = $obj_arr[6];
		 $long = $obj_arr[7];
		 $serial_number = $obj_arr[8];
		 $status = preg_replace("/\r|\n/", "", $obj_arr[9]);	
		 
		 $location = array("lat"=>$lat, "long"=>$long);
		 $reader_arr_itm = array("id"=>$id, "node_id"=>$node_id, "device_type"=>$device_type, "device_sw_version"=>$device_sw_version, "ip_address"=>$ip_address, "label"=>$label, "location"=> $location,"serial_number"=>$serial_number, "status"=>$status);
		 array_push($reader_arr, $reader_arr_itm);
		}
		return $reader_arr;
	}
	
	public function findReader($id){
		if($id > 1000){
		$obj_lines = file("./db/reader_dim_data");
		}
		else{
		$obj_lines = file("./db/reader_dim");		
		}			
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
			if($obj_arr[0] == $id){
			 $id = (int) $obj_arr[0];
			 $node_id = $obj_arr[1];
			 $device_type = $obj_arr[2];
			 $device_sw_version = ($obj_arr[3] === "") ? null : $obj_arr[3];
			 $ip_address = $obj_arr[4];
			 $label = $obj_arr[5];
			 $lat = $obj_arr[6];
			 $long = $obj_arr[7];
			 $serial_number = $obj_arr[8];
			 $status = preg_replace("/\r|\n/", "", $obj_arr[9]);		 
			 break;
			}			
		}		
		if($node_id){
			$location = array("lat"=>$lat, "long"=>$long);
		 $reader_arr = array("id"=>$id, "node_id"=>$node_id, "device_type"=>$device_type, "device_sw_version"=>$device_sw_version, "ip_address"=>$ip_address, "label"=>$label, "location"=> $location,"serial_number"=>$serial_number, "status"=>$status);
			}
			else{
			$reader_arr = array("message"=>"Error! No reader found");
			}		
		return $reader_arr;
	}
	
	public function findLimitReaders($limit){
		$obj_lines = file("./db/reader_dim");
		$sliced_arr = array_slice($obj_lines,0,$limit);
		$reader_arr=array();
		foreach($sliced_arr as $single_line){
		$obj_arr = explode("|", $single_line);			
		 $id = (int) $obj_arr[0];
		 $node_id = $obj_arr[1];
		 $device_type = $obj_arr[2];
		 $device_sw_version = ($obj_arr[3] === "") ? null : $obj_arr[3];
		 $ip_address = $obj_arr[4];
		 $label = $obj_arr[5];
		 $lat = $obj_arr[6];
		 $long = $obj_arr[7];
		 $serial_number = $obj_arr[8];
		 $status = preg_replace("/\r|\n/", "", $obj_arr[9]);			 	
		
		$location = array("lat"=>$lat, "long"=>$long);
		 $reader_arr_itm = array("id"=>$id, "node_id"=>$node_id, "device_type"=>$device_type, "device_sw_version"=>$device_sw_version, "ip_address"=>$ip_address, "label"=>$label, "location"=> $location,"serial_number"=>$serial_number, "status"=>$status);
		 array_push($reader_arr, $reader_arr_itm);						
		}	
		return $reader_arr;
	}
	
	
	public function insertReader(Array $input){
		$obj_lines = file("./db/reader_dim_data");
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];
		}
		if(!$id_num or $id_num == "")
		{ $id_num = 1001; }
		else{ $id_num ++;}
		
		 $node_id = $input['node_id'];
		 $device_type = $input['device_type'];
		 $device_sw_version = $input['device_sw_version'];
		 $ip_address = $input['ip_address'];		 
		 $label = $input['label'];
		 $lat = $input['location']['lat'];
		 $long = $input['location']['long'];
		 $serial_number = $input['serial_number'];		 
		 $status = $input['status'];
		 
	$file_data = fopen("./db/reader_dim_data", "a");
    fwrite($file_data, "$id_num|$node_id|$device_type|$device_sw_version|$ip_address|$label|$lat|$long|$serial_number|$status\n");
    fclose($file_data);
    return array("message"=>"Success", "id"=>$id_num);		
	}
	
	public function updateReader($id, Array $input){
		$obj_lines = file("./db/reader_dim_data");
		$new_obj = fopen("./db/reader_dim_data", "w");			
		 $node_id = $input['node_id'];
		 $device_type = $input['device_type'];
		 $device_sw_version = $input['device_sw_version'];
		 $ip_address = $input['ip_address'];		 
		 $label = $input['label'];
		 $lat = $input['location']['lat'];
		 $long = $input['location']['long'];
		 $serial_number = $input['serial_number'];		 
		 $status = $input['status'];
		
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];		 
		 if($id_num == $id){	
             $res =  fwrite($new_obj, "$id_num|$node_id|$device_type|$device_sw_version|$ip_address|$label|$lat|$long|$serial_number|$status\n");
			}else{
			 fwrite($new_obj, "$single_line");
			}
		 }
		 fclose($new_obj);
		 if($res){
			return array("message"=>"update successful");	
		 }
		 else{
			return array("message"=>"Error! nothing to update");	 
		 }		
	 }
	 
	  public function patchReader($id){
		$obj_lines = file("./db/reader_dim_data");
		$new_obj = fopen("./db/reader_dim_data", "w");				
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		$id_num = $obj_arr[0];		
		if($id_num == $id){	
		   $status = preg_replace("/\r|\n/", "", $obj_arr[9]);	
		   if($status =="online"){
			   $status = "offline";
		   }
		   else{$status = "online";}
		   $res =  fwrite($new_obj, "$obj_arr[0]|$obj_arr[1]|$obj_arr[2]|$obj_arr[3]|$obj_arr[4]|$obj_arr[5]|$obj_arr[6]|$obj_arr[7]|$obj_arr[8]|$status\n");
			}else{
			 fwrite($new_obj, "$single_line");
			}
		 }
		 fclose($new_obj);
		 if($res){
			return array("message"=>"device is now ".$status);	
		 }
		 else{
			return array("message"=>"Error! nothing to activate/de-activate");	 
		 }		 
	 }
	 
	 public function removeReader($id){
		$obj_lines = file("./db/reader_dim_data");
		$new_obj = fopen("./db/reader_dim_data", "w");				
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];		 
		 if($id_num != $id){	
             $res = fwrite($new_obj, "$single_line");
			}
		 }
		 fclose($new_obj);
		 if($res){
			return array("message"=>"delete successful");	
		 }
		 else{
			return array("message"=>"Error! nothing to delete");	 
		 }		 
	 }
}
?>