<?php
class CardDao {

	public function findAllCards(){
		$obj_lines = file("./db/card_dim");
		$card_arr=array();
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id = (int) $obj_arr[0];
		 $customer_id = (int) $obj_arr[1];
		 $brand = $obj_arr[2];
		 $funding = $obj_arr[3];
		 $name = $obj_arr[4];
		 $exp_month = (int) $obj_arr[5];
		 $exp_year = (int) $obj_arr[6];
		 $last_four = (int) $obj_arr[7];
		 $cvv_check = $obj_arr[8];
		 $finger_print = $obj_arr[9];
		 $country = $obj_arr[10];
		 $currency = preg_replace("/\r|\n/", "", $obj_arr[11]);
		 $card_arr_itm = array("id"=>$id, "customer_id"=>$customer_id, "brand"=>$brand, "funding"=>$funding, "name"=>$name, "exp_month"=>$exp_month, "exp_year"=>$exp_year, "last_four"=>$last_four, "cvv_check"=>$cvv_check, "finger_print"=>$finger_print, "country_code"=>$country, "currency_code"=>$currency);
		 array_push($card_arr, $card_arr_itm);
		}
		return $card_arr;
	}
	
	public function findCardById($id){
		if($id > 1000){
		 $obj_lines = file("./db/card_dim_data");	
		}
		else{
		 $obj_lines = file("./db/card_dim");	
		}			
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
			if($obj_arr[0] == $id){
			 $id = (int) $obj_arr[0];
			 $customer_id = (int) $obj_arr[1];
			 $brand = $obj_arr[2];
			 $funding = $obj_arr[3];
			 $name = $obj_arr[4];
			 $exp_month = (int) $obj_arr[5];
			 $exp_year = (int) $obj_arr[6];
			 $last_four = (int) $obj_arr[7];
			 $cvv_check = $obj_arr[8];
			 $finger_print = $obj_arr[9];
			 $country = $obj_arr[10];
			 $currency = preg_replace("/\r|\n/", "", $obj_arr[11]);
			 break;
			}			
		}
		
		if($customer_id){
			$card_arr = array("id"=>$id, "customer_id"=>$customer_id, "brand"=>$brand, "funding"=>$funding, "name"=>$name, "exp_month"=>$exp_month, "exp_year"=>$exp_year, "last_four"=>$last_four, "cvv_check"=>$cvv_check, "finger_print"=>$finger_print, "country_code"=>$country, "currency_code"=>$currency);	
			}
			else{
			$card_arr = array("message"=>"Error! No card found");
			}		
		return $card_arr;
	}
	
	public function findCardByLimit($limit){
		$obj_lines = file("./db/card_dim");
		$sliced_arr = array_slice($obj_lines,0,$limit);
		$card_arr=array();
		foreach($sliced_arr as $single_line){
		$obj_arr = explode("|", $single_line);			
		 $id = (int) $obj_arr[0];
		 $customer_id = (int) $obj_arr[1];
		 $brand = $obj_arr[2];
		 $funding = $obj_arr[3];
		 $name = $obj_arr[4];
		 $exp_month = (int) $obj_arr[5];
		 $exp_year = (int) $obj_arr[6];
		 $last_four = (int) $obj_arr[7];
		 $cvv_check = $obj_arr[8];
		 $finger_print = $obj_arr[9];
		 $country = $obj_arr[10];
		 $currency = preg_replace("/\r|\n/", "", $obj_arr[11]);
		
		 $card_arr_itm = array("id"=>$id, "customer_id"=>$customer_id, "brand"=>$brand, "funding"=>$funding, "name"=>$name, "exp_month"=>$exp_month, "exp_year"=>$exp_year, "last_four"=>$last_four, "cvv_check"=>$cvv_check, "finger_print"=>$finger_print, "country_code"=>$country, "currency_code"=>$currency);
			 array_push($card_arr, $card_arr_itm);						
		}	
		return $card_arr;
	}
	
	public function findCardsforCustomer($cust_id){
		$obj_lines = file("./db/card_dim");
		$card_arr=array();
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		if($obj_arr[1] == $cust_id){
		 $id = (int) $obj_arr[0];
		 $customer_id = (int) $obj_arr[1];
		 $brand = $obj_arr[2];
		 $funding = $obj_arr[3];
		 $name = $obj_arr[4];
		 $exp_month = (int) $obj_arr[5];
		 $exp_year = (int) $obj_arr[6];
		 $last_four = (int) $obj_arr[7];
		 $cvv_check = $obj_arr[8];
		 $finger_print = $obj_arr[9];
		 $country = $obj_arr[10];
		 $currency = preg_replace("/\r|\n/", "", $obj_arr[11]);
		 $card_arr_itm = array("id"=>$id, "customer_id"=>$customer_id, "brand"=>$brand, "funding"=>$funding, "name"=>$name, "exp_month"=>$exp_month, "exp_year"=>$exp_year, "last_four"=>$last_four, "cvv_check"=>$cvv_check, "finger_print"=>$finger_print, "country_code"=>$country, "currency_code"=>$currency);
		 array_push($card_arr, $card_arr_itm);
		 }
		}
		return $card_arr;
	}
	
	
	public function findCardforCustomer($cust_id, $card_id){
		$obj_lines = file("./db/card_dim");
		$card_arr=array();
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
			if($obj_arr[1] == $cust_id && $obj_arr[0] == $card_id){
			 $id = (int) $obj_arr[0];
			 $customer_id = (int) $obj_arr[1];
			 $brand = $obj_arr[2];
			 $funding = $obj_arr[3];
			 $name = $obj_arr[4];
			 $exp_month = (int) $obj_arr[5];
			 $exp_year = (int) $obj_arr[6];
			 $last_four = (int) $obj_arr[7];
			 $cvv_check = $obj_arr[8];
			 $finger_print = $obj_arr[9];
			 $country = $obj_arr[10];
			 $currency = preg_replace("/\r|\n/", "", $obj_arr[11]);
			 break;
			}
		}
		if($customer_id){
			$card_arr = array("id"=>$id, "customer_id"=>$customer_id, "brand"=>$brand, "funding"=>$funding, "name"=>$name, "exp_month"=>$exp_month, "exp_year"=>$exp_year, "last_four"=>$last_four, "cvv_check"=>$cvv_check, "finger_print"=>$finger_print, "country_code"=>$country, "currency_code"=>$currency);	
		}
		else{
			$card_arr = array("message"=>"Error! No card found for this customer");
		}		
		return $card_arr;
	}
	
	public function insertCard(Array $input){
		$obj_lines = file("./db/card_dim_data");
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];
		}
		if(!$id_num or $id_num == "")
		{ $id_num = 1001; }
		else{ $id_num ++;}
		
		 $customer_id = $input['customer_id'];
		 $brand = $input['brand'];
		 $funding = $input['funding'];
		 $name = $input['name'];		 
		 $exp_month = $input['exp_month'];
		 $exp_year = $input['exp_year'];
		 $last_four = $input['last_four'];
		 $cvv_check = $input['cvv_check'];
		 $finger_print = $input['finger_print'];
		 $country_code = $input['country_code'];
		 $currency_code = $input['currency_code'];
		 
	$file_data = fopen("./db/card_dim_data", "a");
    fwrite($file_data, "$id_num|$customer_id|$brand|$funding|$name|$exp_month|$exp_year|$last_four|$cvv_check|$finger_print|$country_code|$currency_code\n");
    fclose($file_data);
    return array("message"=>"Success", "id"=>$id_num);	
	}
	
	public function updateCard($id, Array $input){
		$obj_lines = file("./db/card_dim_data");
		$new_obj = fopen("./db/card_dim_data", "w");			
		 $customer_id = $input['customer_id'];
		 $brand = $input['brand'];
		 $funding = $input['funding'];
		 $name = $input['name'];		 
		 $exp_month = $input['exp_month'];
		 $exp_year = $input['exp_year'];
		 $last_four = $input['last_four'];
		 $cvv_check = $input['cvv_check'];
		 $finger_print = $input['finger_print'];
		 $country_code = $input['country_code'];
		 $currency_code = $input['currency_code'];
		 
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];		 
		 if($id_num == $id){	
             $res = fwrite($new_obj, "$id_num|$customer_id|$brand|$funding|$name|$exp_month|$exp_year|$last_four|$cvv_check|$finger_print|$country_code|$currency_code\n");
			}else{
			 fwrite($new_obj, "$single_line");
			}
		 }
		 fclose($new_obj);
		 if($res){
			return array("message"=>"update successful");	
		 }
		 else{
			return array("message"=>"Error! nothing to update");	 
		 }		
	 }
	 
	 
	  public function removeCard($id){
		$obj_lines = file("./db/card_dim_data");
		$new_obj = fopen("./db/card_dim_data", "w");				
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];		 
		 if($id_num != $id){	
             $res = fwrite($new_obj, "$single_line");
			}
		 }
		 fclose($new_obj);
		 if($res){
			return array("message"=>"delete successful");	
		 }
		 else{
			return array("message"=>"Error! nothing to delete");	 
		 }		 
	 }
}
?>