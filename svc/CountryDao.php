<?php
class CountryDao {

	public function findAllCounties(){
		$obj_lines = file("./db/country_master");
		$country_arr=array();
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id = (int) $obj_arr[0];
		 $country = $obj_arr[1];
		 $iso_code = $obj_arr[2];
		 $area = $obj_arr[3];
		 $currency = $obj_arr[4];
		 $currency_code = $obj_arr[5];
		 $calling_code = $obj_arr[6];
		 $flag = preg_replace("/\r|\n/", "", $obj_arr[7]);
		 $country_arr_itm = array("id"=>$id, "country"=>$country, "abbreviation"=>$iso_code, "area"=>$area, "currency"=>$currency, "currency_code"=>$currency_code, "calling_code"=>$calling_code);
		 array_push($country_arr, $country_arr_itm);
		}
		return $country_arr;
	}
	
	public function findCountryById($id){
		$obj_lines = file("./db/country_master");		
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
			if($obj_arr[0] == $id){
			 $id = (int) $obj_arr[0];
			 $country = $obj_arr[1];
			 $iso_code = $obj_arr[2];
			 $area = $obj_arr[3];
			 $currency = $obj_arr[4];
			 $currency_code = $obj_arr[5];
			 $calling_code = $obj_arr[6];
			 $flag = preg_replace("/\r|\n/", "", $obj_arr[7]);
			 break;
			}			
		}
		
		if($iso_code){
			$country_arr = array("id"=>$id, "country"=>$country, "abbreviation"=>$iso_code, "area"=>$area, "currency"=>$currency, "currency_code"=>$currency_code, "calling_code"=>$calling_code);	
			}
			else{
			$country_arr = array("message"=>"Error! No country found");
			}		
		return $country_arr;
	}
	
	public function findCountryByLimit($limit){
		$obj_lines = file("./db/country_master");
		$sliced_arr = array_slice($obj_lines,0,$limit);
		$country_arr=array();
		foreach($sliced_arr as $single_line){
		$obj_arr = explode("|", $single_line);			
			 $id = (int) $obj_arr[0];
			 $country = $obj_arr[1];
			 $iso_code = $obj_arr[2];
			 $area = $obj_arr[3];
			 $currency = $obj_arr[4];
			 $currency_code = $obj_arr[5];
			 $calling_code = $obj_arr[6];
			 $flag = preg_replace("/\r|\n/", "", $obj_arr[7]);
			 $country_arr_itm = array("id"=>$id, "country"=>$country, "abbreviation"=>$iso_code, "area"=>$area, "currency"=>$currency, "currency_code"=>$currency_code, "calling_code"=>$calling_code);
			 array_push($country_arr, $country_arr_itm);						
		}	
		return $country_arr;
	}
}
?>