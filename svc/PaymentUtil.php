<?php

class PaymentUtil{
	
	public function generatePayCode(){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$code = '';
		for ($i = 0; $i < 11; $i++) {
			$code .= $characters[rand(0, $charactersLength - 1)];
		}
		return 'pay_'.$code;	
	}
	
	public function calcFeeAmt($amount){
		$num_data = rand(10,100)/10;
		$free_amt = number_format((float)$num_data, 2, '.', '');
		return $free_amt;
	}
	
	public function findStatus(){
		$status= "succeeded,confirmed,processing";
		$statusArray = explode(',', $status);
		$val = $statusArray[mt_rand(0, count($statusArray))];
		return $val;
	}
}
?>