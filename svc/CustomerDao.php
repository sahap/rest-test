<?php
class CustomerDao {

	public function findAllCustomers(){
		$obj_lines = file("./db/customer_dim");
		$customer_arr=array();
		$cust_arr_ship = array();
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id = (int) $obj_arr[0];
		 $code =  $obj_arr[1];
		 $name =  $obj_arr[2];
		 $email = $obj_arr[3];
		 $phone =  $obj_arr[4];		
		 $street = $obj_arr[5];
		 $city = $obj_arr[6];
		 $postal_code = $obj_arr[7];
		 $country = $obj_arr[8];
		 
		 $name_ship = $obj_arr[9];
		 $phone_ship = $obj_arr[10];
		 $street_ship = $obj_arr[11];
		 $city_ship = $obj_arr[12];
		 $postal_code_ship = $obj_arr[13];
		 $country_ship = $obj_arr[14];
		 
		 $currency = $obj_arr[15];
		 $tax_exempt = $obj_arr[16];
		 $is_active = preg_replace("/\r|\n/", "", $obj_arr[17]);
		 
		 if($name_ship){
		  $cust_arr_ship = array("name"=> $name_ship, "phone"=> $phone_ship, "street"=>$street_ship, "city"=>$city_ship, "postal_code"=>$postal_code_ship, "country_code"=>$country_ship);
		 }
		 else{
			 $cust_arr_ship = null;
		 }
		 $cust_arr_addr = array("street"=> $street, "city"=> $city, "postal_code"=> $postal_code, "country_code"=> $country);		 
		 $cust_arr_itm = array("id"=> $id, "code"=>$code, "name"=> $name, "email"=> $email, "phone"=> $phone, "address"=> $cust_arr_addr, "shipping"=>$cust_arr_ship, "currency_code"=>$currency, "tax_exempt"=>$tax_exempt, "is_active"=>$is_active);
		 array_push($customer_arr, $cust_arr_itm);
		}
		return $customer_arr;
	}
	
	public function findCustomerById($id){
		$cust_arr_ship = array();
		$obj_lines = file("./db/customer_dim");		
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
			if($obj_arr[0] == $id){
			 $id = (int) $obj_arr[0];
			 $code =  $obj_arr[1];
			 $name =  $obj_arr[2];
			 $email = $obj_arr[3];
			 $phone =  $obj_arr[4];		
			 $street = $obj_arr[5];
			 $city = $obj_arr[6];
			 $postal_code = $obj_arr[7];
			 $country = $obj_arr[8];
			 
			 $name_ship = $obj_arr[9];
			 $phone_ship = $obj_arr[10];
			 $street_ship = $obj_arr[11];
			 $city_ship = $obj_arr[12];
			 $postal_code_ship = $obj_arr[13];
			 $country_ship = $obj_arr[14];
			 
			 $currency = $obj_arr[15];
			 $tax_exempt = $obj_arr[16];
			 $is_active = preg_replace("/\r|\n/", "", $obj_arr[17]);
			 break;
			}
		}
		if($name_ship){
		  $cust_arr_ship = array("name"=> $name_ship, "phone"=> $phone_ship, "street"=>$street_ship, "city"=>$city_ship, "postal_code"=>$postal_code_ship, "country_code"=>$country_ship);
		 }
		  else{
			 $cust_arr_ship = null;
		 }
		 
		 $cust_arr_addr = array("street"=> $street, "city"=> $city, "postal_code"=> $postal_code, "country_code"=> $country);		 
		 $customer_arr = array("id"=> $id, "code"=>$code, "name"=> $name, "email"=> $email, "phone"=> $phone, "address"=> $cust_arr_addr, "shipping"=>$cust_arr_ship, "currency_code"=>$currency, "tax_exempt"=>$tax_exempt, "is_active"=>$is_active);
		return $customer_arr;
	}
	
	public function findCustomerByLimit($limit){
		$obj_lines = file("./db/customer_dim");
		$sliced_arr = array_slice($obj_lines,0,$limit);
		$cust_arr_ship = array();
		$customer_arr=array();
		foreach($sliced_arr as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id = (int) $obj_arr[0];
		 $code =  $obj_arr[1];
		 $name =  $obj_arr[2];
		 $email = $obj_arr[3];
		 $phone =  $obj_arr[4];		
		 $street = $obj_arr[5];
		 $city = $obj_arr[6];
		 $postal_code = $obj_arr[7];
		 $country = $obj_arr[8];
		 
		 $name_ship = $obj_arr[9];
		 $phone_ship = $obj_arr[10];
		 $street_ship = $obj_arr[11];
		 $city_ship = $obj_arr[12];
		 $postal_code_ship = $obj_arr[13];
		 $country_ship = $obj_arr[14];
		 
		 $currency = $obj_arr[15];
		 $tax_exempt = $obj_arr[16];
		 $is_active = preg_replace("/\r|\n/", "", $obj_arr[17]);
			 
		if($name_ship){
		  $cust_arr_ship = array("name"=> $name_ship, "phone"=> $phone_ship, "street"=>$street_ship, "city"=>$city_ship, "postal_code"=>$postal_code_ship, "country_code"=>$country_ship);
		 }
		 else{
			 $cust_arr_ship = null;
		 }
		 
		 $cust_arr_addr = array("street"=> $street, "city"=> $city, "postal_code"=> $postal_code, "country_code"=> $country);		 
		 $cust_arr_itm = array("id"=> $id, "code"=>$code, "name"=> $name, "email"=> $email, "phone"=> $phone, "address"=> $cust_arr_addr, "shipping"=>$cust_arr_ship, "currency_code"=>$currency, "tax_exempt"=>$tax_exempt, "is_active"=>$is_active);
		 array_push($customer_arr, $cust_arr_itm);
		}
		return $customer_arr;
	}
}
?>