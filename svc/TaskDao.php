<?php
class TaskDao {

	public function findAllTasks(){
		$obj_lines = file("./db/task_fact");
		$task_arr=array();
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id = (int) $obj_arr[0];
		 $user_id = (int) $obj_arr[1];
		 $name = $obj_arr[2];
		 $description = $obj_arr[3];
		 $created = (int)$obj_arr[4];
		 $updated = ($obj_arr[5] === "") ? null : (int)$obj_arr[5];
		 $status = $obj_arr[6];
		 $related_task_id = ($obj_arr[7] === "") ? null : (int)$obj_arr[7];
		 $privacy = $obj_arr[8];
		 $priority = preg_replace("/\r|\n/", "", $obj_arr[9]);	
		 $priority = ($priority === "") ? null : $priority;
		 $task_arr_itm = array("id"=>$id, "user_id"=>$user_id, "name"=>$name, "description"=>$description, "created"=>$created, "updated"=>$updated, "status"=>$status, "related_task_id"=>$related_task_id, "privacy"=>$privacy, "priority"=>$priority);
		 array_push($task_arr, $task_arr_itm);
		}
		return $task_arr;
	}
	
	public function findTask($id){
		if($id > 1000){
		$obj_lines = file("./db/task_fact_data");
		}
		else{
		$obj_lines = file("./db/task_fact");		
		}			
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
			if($obj_arr[0] == $id){
			 $id = (int) $obj_arr[0];
			 $user_id = (int) $obj_arr[1];
			 $name = $obj_arr[2];
			 $description = $obj_arr[3];
			 $created = (int)$obj_arr[4];
			 $updated = ($obj_arr[5] === "") ? null : (int)$obj_arr[5];
			 $status = $obj_arr[6];
			 $related_task_id = ($obj_arr[7] === "") ? null : (int)$obj_arr[7];
			 $privacy = $obj_arr[8];
			 $priority = preg_replace("/\r|\n/", "", $obj_arr[9]);	
			 $priority = ($priority === "") ? null : $priority;			 
			 break;
			}			
		}		
		if($user_id){
			$task_arr = array("id"=>$id, "user_id"=>$user_id, "name"=>$name, "description"=>$description, "created"=>$created, "updated"=>$updated, "status"=>$status, "related_task_id"=>$related_task_id, "privacy"=>$privacy, "priority"=>$priority);
			}
			else{
			$task_arr = array("message"=>"Error! No task found");
			}		
		return $task_arr;
	}
	
	public function findLimitTasks($limit){
		$obj_lines = file("./db/task_fact");
		$sliced_arr = array_slice($obj_lines,0,$limit);
		$task_arr=array();
		foreach($sliced_arr as $single_line){
		$obj_arr = explode("|", $single_line);			
		  $id = (int) $obj_arr[0];
		 $user_id = (int) $obj_arr[1];
		 $name = $obj_arr[2];
		 $description = $obj_arr[3];
		 $created = (int)$obj_arr[4];
		 $updated = ($obj_arr[5] === "") ? null : (int)$obj_arr[5];
		 $status = $obj_arr[6];
		 $related_task_id = ($obj_arr[7] === "") ? null : (int)$obj_arr[7];
		 $privacy = $obj_arr[8];
		 $priority = preg_replace("/\r|\n/", "", $obj_arr[9]);	
		 $priority = ($priority === "") ? null : $priority;
		
		 $task_arr_itm = array("id"=>$id, "user_id"=>$user_id, "name"=>$name, "description"=>$description, "created"=>$created, "updated"=>$updated, "status"=>$status, "related_task_id"=>$related_task_id, "privacy"=>$privacy, "priority"=>$priority);
			 array_push($task_arr, $task_arr_itm);						
		}	
		return $task_arr;
	}
	
	
	public function insertTask(Array $input){
		$obj_lines = file("./db/task_fact_data");
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];
		}
		if(!$id_num or $id_num == "")
		{ $id_num = 1001; }
		else{ $id_num ++;}
		
		 $user_id = $input['user_id'];
		 $name = $input['name'];
		 $description = $input['description'];
		 $created = time();
		 $status = $input['status'];
		 $related_task_id = $input['related_task_id'];
		 $privacy = $input['privacy'];
		 $priority = $input['priority'];
		 
	$file_data = fopen("./db/task_fact_data", "a");
    fwrite($file_data, "$id_num|$user_id|$name|$description|$created||$status|$related_task_id|$privacy|$priority\n");
    fclose($file_data);
    return array("message"=>"Success", "id"=>$id_num);		
	}
	
	public function updateTask($id, Array $input){
		$obj_lines = file("./db/task_fact_data");
		$new_obj = fopen("./db/task_fact_data", "w");			
		 $user_id = $input['user_id'];
		 $name = $input['name'];
		 $description = $input['description'];
		 $updated = time();
		 $status = $input['status'];
		 $related_task_id = $input['related_task_id'];
		 $privacy = $input['privacy'];
		 $priority = $input['priority'];
		
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];
		 $created = $obj_arr[4];		 
		 if($id_num == $id){	
             $res = fwrite($new_obj, "$id_num|$user_id|$name|$description|$created|$updated|$status|$related_task_id|$privacy|$priority\n");
			}else{
			 fwrite($new_obj, "$single_line");
			}
		 }
		 fclose($new_obj);
		 if($res){
			return array("message"=>"update successful");	
		 }
		 else{
			return array("message"=>"Error! nothing to update");	 
		 }		
	 }
	 
	 public function removeTask($id){
		$obj_lines = file("./db/task_fact_data");
		$new_obj = fopen("./db/task_fact_data", "w");				
		foreach($obj_lines as $single_line){
		$obj_arr = explode("|", $single_line);
		 $id_num = $obj_arr[0];		 
		 if($id_num != $id){	
             $res = fwrite($new_obj, "$single_line");
			}
		 }
		 fclose($new_obj);
		 if($res){
			return array("message"=>"delete successful");	
		 }
		 else{
			return array("message"=>"Error! nothing to delete");	 
		 }		 
	 }
}
?>