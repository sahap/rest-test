<?php
require 'CardDao.php';
class CardController {

    private $requestMethod;
    private $id;
	private $limit;

	public function __construct($requestMethod, $id, $limit)
    {        
        $this->requestMethod = $requestMethod;
        $this->id = $id;
		$this->limit = $limit;
    }
	
    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->id) {
                    $response = $this->getCard($this->id);
                }								
				else if ($this->limit) {
                    $response = $this->getLimitCard($this->limit);
                } 
				else {
                    $response = $this->getAllCards();
                };
                break;            
           case 'POST':
                $response = $this->createCardFromRequest();
                break;
			case 'PUT':
                $response = $this->updateCardFromRequest($this->id);
                break;
			case 'DELETE':
                $response = $this->deleteCard($this->id);
                break;
			default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllCards()
    {
		$dao = new CardDao();		
        $result = $dao->findAllCards();// find all countries
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }

    private function getCard($id)
    {
		$dao = new CardDao();		
        $result = $dao->findCardById($id);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    } 
	
	private function getLimitCard($limit){
		$dao = new CardDao();		
        $result = $dao->findCardByLimit($limit);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
	}	
	
    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
	
	// POST
	 private function createCardFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateCard($input)) {
            return $this->unprocessableEntityResponse();
        }
		$dao = new CardDao();
		$result = $dao->insertCard($input); 
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	// PUT
	 private function updateCardFromRequest($id)
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (!$id) {
            return $this->unprocessableEntityResponse();
        }
		$dao = new CardDao();
		$result = $dao->updateCard($id, $input); 
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	// DELETE
	 private function deleteCard($id)
    {  
		$dao = new CardDao();
		$result = $dao->removeCard($id); 
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
         $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	
	 private function validateCard($input){
        if (! isset($input['customer_id'])) {
            return false;
        }
        if (! isset($input['brand'])) {  
            return false;
        }
		if (! isset($input['funding'])) {  
            return false;
        }
		if (! isset($input['name'])) {  
            return false;
        }
		if (! isset($input['exp_month'])) {  
            return false;
        }
		if (! isset($input['exp_year'])) {  
            return false;
        }
		if (! isset($input['last_four'])) {  
            return false;
        }
		if (! isset($input['cvv_check'])) {  
            return false;
        }
		if (! isset($input['finger_print'])) {  
            return false;
        }
		if (! isset($input['country_code'])) {  
            return false;
        }
		if (! isset($input['currency_code'])) {  
		    return false;
        }
        return true;
    }
	
	 private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }
}