<?php
require __DIR__.'/../svc/UserDao.php';
class UserController {

    private $requestMethod;
    private $id;
	private $limit;

	public function __construct($requestMethod, $id, $limit)
    {        
        $this->requestMethod = $requestMethod;
        $this->id = $id;
		$this->limit = $limit;
    }
	
    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->id) {
                    $response = $this->getUser($this->id);
                } 
				else if ($this->limit) {
                    $response = $this->getLimitUser($this->limit);
                } 
				else {
                    $response = $this->getAllUsers();
                };
                break;            
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllUsers()
    {
		$dao = new UserDao();		
        $result = $dao->findAllUsers();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }

    private function getUser($id)
    {
		$dao = new UserDao();		
        $result = $dao->findUserById($id);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    } 
	
	private function getLimitUser($limit){
		$dao = new UserDao();		
        $result = $dao->findUserByLimit($limit);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
	}

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}