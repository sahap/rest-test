<?php
require 'PaymentDao.php';
class PaymentController {

    private $requestMethod;
    private $id;

	public function __construct($requestMethod, $id)
    {        
        $this->requestMethod = $requestMethod;
        $this->id = $id;
    }
	
    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->id) {
                    $response = $this->getPayment($this->id);
                }
				else {
                    $response = $this->getAllPayments();
                };
                break; 
			case 'POST':
                $response = $this->createPaymentFromRequest();
                break;
			case 'PATCH':
                $response = $this->cancelPayment($this->id);
                break;				
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllPayments()
    {
		$dao = new PaymentDao();		
        $result = $dao->findAllPayments();// find all countries
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }

    private function getPayment($id)
    {
		$dao = new PaymentDao();		
        $result = $dao->findPaymentById($id);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }	
	
    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
	
	// PATCH
	 private function cancelPayment($id)
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (!$id) {
            return $this->unprocessableEntityResponse();
        }
		$dao = new PaymentDao();
		$result = $dao->cancelPayment($id, $input); 
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
		
	// POST
	 private function createPaymentFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validatePayment($input)) {
            return $this->unprocessableEntityResponse();
        }
		$dao = new PaymentDao();
		$result = $dao->insertPayment($input); 
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	private function validatePayment($input){
        if (! isset($input['customer_id'])) {
            return false;
        }
        if (! isset($input['amount'])) {  
            return false;
        }
		if (! isset($input['currency_code'])) {  
            return false;
        }
		if (! isset($input['description'])) {  
            return false;
        }
		if (! isset($input['receipt_email'])) {  
            return false;
        }
		if (! isset($input['transfer_data']['destination'])) {  
            return false;
        }
        return true;
    }
	
	 private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }
}