<?php
require 'TaskDao.php';
class TaskController {

    private $requestMethod;
    private $id;
	private $limit;

	public function __construct($requestMethod, $id, $limit)
    {        
        $this->requestMethod = $requestMethod;
        $this->id = $id;
		$this->limit = $limit;
    }
	
    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->id) {
                    $response = $this->getTask($this->id);
                }								
				else if ($this->limit) {
                    $response = $this->getLimitTasks($this->limit);
                } 
				else {
                    $response = $this->getAllTasks();
                };
                break;
			case 'POST':
                $response = $this->createTaskFromRequest();
                break;
			case 'PUT':
                $response = $this->updateTaskFromRequest($this->id);
                break;
			case 'DELETE':
                $response = $this->deleteTask($this->id);
                break;
			default:
                $response = $this->notFoundResponse();
                break;
				
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllTasks()
    {
		$dao = new TaskDao();		
        $result = $dao->findAllTasks();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }

    private function getTask($id)
    {
		$dao = new TaskDao();		
        $result = $dao->findTask($id);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    } 
	
	private function getLimitTasks($limit){
		$dao = new TaskDao();		
        $result = $dao->findLimitTasks($limit);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
	}	
	
    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
	
	// POST
	 private function createTaskFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateTask($input)) {
            return $this->unprocessableEntityResponse();
        }
		$dao = new TaskDao();
		$result = $dao->insertTask($input); 
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	// PUT
	 private function updateTaskFromRequest($id)
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (!$id) {
            return $this->unprocessableEntityResponse();
        }
		$dao = new TaskDao();
		$result = $dao->updateTask($id, $input); 
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	// DELETE
	 private function deleteTask($id)
    {  
		$dao = new TaskDao();
		$result = $dao->removeTask($id); 
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
         $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	
	 private function validateTask($input){
        if (! isset($input['user_id'])) {
            return false;
        }
        if (! isset($input['name'])) {  
            return false;
        }
		if (! isset($input['status'])) {  
            return false;
        }
		if (! isset($input['privacy'])) {  
            return false;
        }
        return true;
    }
	
	 private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }
}