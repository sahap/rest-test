<?php
require 'ReaderDao.php';
class ReaderController {

    private $requestMethod;
    private $id;
	private $limit;

	public function __construct($requestMethod, $id, $limit)
    {        
        $this->requestMethod = $requestMethod;
        $this->id = $id;
		$this->limit = $limit;
    }
	
    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->id) {
                    $response = $this->getReader($this->id);
                }								
				else if ($this->limit) {
                    $response = $this->getLimitReaders($this->limit);
                } 
				else {
                    $response = $this->getAllReaders();
                };
                break;
			case 'POST':
                $response = $this->createReaderFromRequest();
                break;
			case 'PUT':
                $response = $this->updateReaderFromRequest($this->id);
                break;
			case 'PATCH':
                $response = $this->activateReader($this->id);
                break;
			case 'DELETE':
                $response = $this->deleteReader($this->id);
                break;
			default:
                $response = $this->notFoundResponse();
                break;
				
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllReaders()
    {
		$dao = new ReaderDao();		
        $result = $dao->findAllReaders();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }

    private function getReader($id)
    {
		$dao = new ReaderDao();		
        $result = $dao->findReader($id);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    } 
	
	private function getLimitReaders($limit){
		$dao = new ReaderDao();		
        $result = $dao->findLimitReaders($limit);        
        if (!$result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
	}	
	
    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
	
	// POST
	 private function createReaderFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateReader($input)) {
            return $this->unprocessableEntityResponse();
        }
		$dao = new ReaderDao();
		$result = $dao->insertReader($input); 
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	// PUT
	 private function updateReaderFromRequest($id)
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (!$id) {
            return $this->unprocessableEntityResponse();
        }
		$dao = new ReaderDao();
		$result = $dao->updateReader($id, $input); 
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	
	// PATCH
	 private function activateReader($id)
    {       
        if (!$id) {
            return $this->unprocessableEntityResponse();
        }
		$dao = new ReaderDao();
		$result = $dao->patchReader($id); 
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	// DELETE
	 private function deleteReader($id)
    {  
		$dao = new ReaderDao();
		$result = $dao->removeReader($id); 
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
         $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	
	 private function validateReader($input){
        if (! isset($input['node_id'])) {
            return false;
        }
        if (! isset($input['device_type'])) {  
            return false;
        }
		if (! isset($input['ip_address'])) {  
            return false;
        }
		if (! isset($input['label'])) {  
            return false;
        }
		if (! isset($input['location']['lat'])) {  
            return false;
        }
		if (! isset($input['location']['long'])) {  
            return false;
        }
		if (! isset($input['serial_number'])) {  
            return false;
        }		
        return true;
    }
	
	 private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }
}