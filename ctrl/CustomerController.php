<?php
require 'CustomerDao.php';
require 'CardDao.php';

class CustomerController {

    private $requestMethod;
    private $id;
	private $limit;
	private $search_cards;
	private $card_id;

	public function __construct($requestMethod, $id, $limit, $search_cards, $card_id)
    {        
        $this->requestMethod = $requestMethod;
        $this->id = $id;
		$this->limit = $limit;
		$this->search_cards = $search_cards;
		$this->card_id = $card_id;
    }
	
    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if($this->id && $this->search_cards) {
                    $response = $this->getCardsforCustomer($this->id);
					break;
                } 
				else if($this->id && $this->card_id) {
                    $response = $this->getCardforCustomer($this->id, $this->card_id);
					break;
                } 
				else if($this->id) {
                    $response = $this->getCustomer($this->id);
                } 
				else if ($this->limit) {
                    $response = $this->getLimitCustomer($this->limit);
                }				
				else {
                    $response = $this->getAllCustomers();
                };
                break;            
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllCustomers()
    {
		$dao = new CustomerDao();		
        $result = $dao->findAllCustomers();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }

    private function getCustomer($id)
    {
		$dao = new CustomerDao();		
        $result = $dao->findCustomerById($id);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    } 
	
	private function getLimitCustomer($limit){
		$dao = new CustomerDao();		
        $result = $dao->findCustomerByLimit($limit);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
	}
	
	private function getCardsforCustomer($id){
		$dao = new CardDao();		
        $result = $dao->findCardsforCustomer($id);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
	}
	
	private function getCardforCustomer($id, $card_id){
		$dao = new CardDao();		
        $result = $dao->findCardforCustomer($id, $card_id);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
	}

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}