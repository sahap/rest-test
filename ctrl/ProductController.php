<?php
require 'ProductDao.php';
class ProductController {

    private $requestMethod;
    private $id;
	private $limit;

	public function __construct($requestMethod, $id, $limit)
    {        
        $this->requestMethod = $requestMethod;
        $this->id = $id;
		$this->limit = $limit;
    }
	
    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->id) {					
                    $response = $this->getProduct($this->id);
                }								
				else if ($this->limit) {
                    $response = $this->getLimitProducts($this->limit);
                } 
				else {
                    $response = $this->getAllProducts();
                };
                break;
			case 'POST':
                $response = $this->createProductFromRequest();
                break;
			case 'PATCH':
                $response = $this->patchProduct($this->id);
                break;
			case 'DELETE':
                $response = $this->deleteProduct($this->id);
                break;
			default:
                $response = $this->notFoundResponse();
                break;
				
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllProducts()
    {
		$dao = new ProductDao();		
        $result = $dao->findAllProducts();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }

    private function getProduct($id)
    {
		$dao = new ProductDao();
        $result = $dao->findProduct($id);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    } 
	
	private function getLimitProducts($limit){
		$dao = new ProductDao();		
        $result = $dao->findLimitProducts($limit);        
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
	}	
	
    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
	
	// POST
	 private function createProductFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateProduct($input)) {
            return $this->unprocessableEntityResponse();
        }
		$dao = new ProductDao();
		$result = $dao->insertProduct($input); 
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	// PUT
	 private function patchProduct($id)
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (!$id) {
            return $this->unprocessableEntityResponse();
        }
		$dao = new ProductDao();
		$result = $dao->updateProduct($id, $input); 
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	// DELETE
	 private function deleteProduct($id)
    {  
		$dao = new ProductDao();
		$result = $dao->removeProduct($id); 
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
         $response['body'] = json_encode($result, JSON_PRETTY_PRINT);
        return $response;
    }
	
	
	 private function validateProduct($input){
        if (! isset($input['active'])) {
            return false;
        }
        if (! isset($input['name'])) {  
            return false;
        }
		if (! isset($input['currency'])) {  
            return false;
        }
		if (! isset($input['inventory']['quantity'])) {  
            return false;
        }
		if (! isset($input['inventory']['type'])) {  
            return false;
        }
		if (! isset($input['inventory']['value'])) {  
            return false;
        }
		if (! isset($input['package_dimensions']['weight'])) {  
            return false;
        }
		if (! isset($input['price'])) {  
            return false;
        }
		if (! isset($input['season_label'])) {  
            return false;
        }
        return true;
    }
	
	 private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }
}