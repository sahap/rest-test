<?php
require 'PaymentController.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,PATCH,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );

// all of our endpoints start with /users
// everything else results in a 404 Not Found
if ($uri[1] !== 'payments') {
    header("HTTP/1.1 404 Not Found");
    exit();
}

// the id is, of course, optional
$id = null;
if (isset($uri[2])) {
    $id = (int)$uri[2];
}

$requestMethod = $_SERVER["REQUEST_METHOD"];

// pass the request method and id to the CountryController and process the HTTP request:
$controller = new PaymentController($requestMethod, $id);
$controller->processRequest();
?>