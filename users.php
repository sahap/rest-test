<?php
require __DIR__.'/ctrl/UserController.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );
// all of our endpoints start with /users
// everything else results in a 404 Not Found
if ($uri[2] !== 'users') {
    header("HTTP/1.1 404 Not Found");
    exit();
}

// the id is, of course, optional and must be a number:
$id = null;
if (isset($uri[3])) {
    $id = (int) $uri[3];
}

// find the top limit, of course, optional and must be a number:
$limit = null;
if($uri[3] == 'top' && isset($uri[4])){
	 $limit = (int) $uri[4];
}

$requestMethod = $_SERVER["REQUEST_METHOD"];

// pass the request method, id and limit to the UserController and process the HTTP request:
$controller = new UserController($requestMethod, $id, $limit);
$controller->processRequest();
?>