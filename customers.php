<?php
require 'CustomerController.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );

// all of our endpoints start with /users
// everything else results in a 404 Not Found
if ($uri[1] !== 'customers') {
    header("HTTP/1.1 404 Not Found");
    exit();
}

// the id is, of course, optional and must be a number:
$id = null;
if (isset($uri[2])) {
    $id = (int) $uri[2];
}


$limit = null;
if($uri[2] == 'top' && isset($uri[3])){
	 $limit = (int) $uri[3];
}

$search_cards = null; // has customer id, cards
if(isset($uri[2]) && $uri[3] == 'cards' && !isset($uri[4])){
	$search_cards = 1;
}

$card_id = null; // has customer id, cards, and card id
if(isset($uri[2]) && $uri[3] == 'cards' && isset($uri[4])){
	 $id = (int) $uri[2];
	 $card_id = (int) $uri[4];
}

$requestMethod = $_SERVER["REQUEST_METHOD"];

// pass the request method and id to the CountryController and process the HTTP request:
$controller = new CustomerController($requestMethod, $id, $limit, $search_cards, $card_id);
$controller->processRequest();
?>