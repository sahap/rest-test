# Mock API #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this? ###
Mock API is a free, open-source dummy API server for simulating and testing.
[Mock API](https://piuli.ga/mockapi)

### What does it look like? ###

Run the following code, in a console
` fetch('https://piuli.ga/mockapi/users/1').then(response => response.json())
`

### How do I use it? ###

Mock API comes with a set of 8 common resources: The resources have relations. For example: users have many tasks, customer have many cards

## Resources ##

* /countries	225 countries
* /users	100 users
* /tasks	100 tasks
* /customers	100 customers
* /cards	200 cards
* /products	100 products
* /payments	100 payments
* /readers	100 readers


### Endpoints ###

* GET	/products	Get all list of products
* GET	/products/1	Get product details for productId = 1
* POST	/products	
* PUT	/products/100	
* PATCH	/products/100	
* DELETE	/products/100	
* GET	/users/1/photos	
* GET	/users/1/photos/1	
* GET	/countries	
* GET	/countries/1	
* GET	/countries?limit=10
